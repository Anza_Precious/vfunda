import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from "../../shared/services/auth.service";
import { Router } from "@angular/router";
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    surname: string;
    firstnames: string;
    lastname: string;
    age:number;
    message:string;
    users:any;
    user:any;
    uid
    myprofile: any;

  constructor(
    public authService: AuthService,
    public router: Router,
    public ngZone: NgZone
    
  ) {
// this.uid= this.authService.afAuth.auth.currentUser.uid
// console.log(this.uid);

   }
ngOnInit(){
  
  this.authService.get_Allusers().subscribe(data => {
  this.myprofile=data.map(e =>{

    return{
id: e.payload.doc.id,
isedit:false,    
lastname: e.payload.doc.data()['lastname'],
firstnames: e.payload.doc.data()['firstnames'],
age:e.payload.doc.data()['age'],
    };
  })
console.log(this.myprofile);
 
})



};
EditRecord(Record){
Record.isedit=true;
Record.firstnames=Record.firstnames;
Record.lastname=Record.lastname;

Record.age=Record.age;



}
Deleteusers(record_id){

  this.authService.delete_users(record_id);
}

  CreateRecord()
  {
    let Record ={};
   Record['firstnames'] = this.firstnames;
   Record['lastname'] = this.lastname;

   Record['age'] = this.age;
  
  this.authService.create_Newuser(Record).then(res =>{
  this.firstnames="";
  this.lastname="";
  this.age=undefined;
console.log(res);
this.message="Users data saved";
   }).catch(error => {
     console.log(error);
 });

   }
Updatearecord(recorddata){
let record={};
record['firstnames'] = recorddata.firstnames
record['age'] = recorddata.age
record['lastname'] = recorddata.lastname

this.authService.update_users(recorddata.id, recorddata );
recorddata.isedit=false;



}


  }


