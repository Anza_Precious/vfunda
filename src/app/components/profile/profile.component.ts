import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { AuthService } from "../../shared/services/auth.service";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  age: number;
  firstnames: string;
  lastname: string;
    title = 'Angular_Firebase';
    description = 'Angular_Firebase';
   uid
   user:any;


    itemValue = '';
    items: any;

    constructor(public db: AngularFireDatabase ,public authService: AuthService, 
       public afs: AngularFirestore,
       public fireservices:AngularFirestore,   
      public afAuth: AngularFireAuth) {
        this.items = db.list(`${this.afAuth.auth.currentUser.uid}`).snapshotChanges();
        // items = db.object(`${this.afAuth.auth.currentUser.uid}`).valueChanges();
  }


  
    onSubmit() {
      let Record ={};
      Record['firstnames'] = this.firstnames;
      Record['lastname'] = this.lastname;
   



      Record['age'] = this.age;
      this.db.list(`${this.afAuth.auth.currentUser.uid}`).set(`${this.afAuth.auth.currentUser.uid}`,Record);
 
      this.firstnames="";
      this.lastname="";
      this.age=undefined;
      this.itemValue = '';
      console.log(Record);

      return this.db.list(`${this.afAuth.auth.currentUser.uid}`).update(`${this.afAuth.auth.currentUser.uid}`,Record);
    }
  
  }
